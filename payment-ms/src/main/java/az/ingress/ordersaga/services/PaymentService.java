package az.ingress.ordersaga.services;

import az.ingress.common.saga.events.CancelShippingEvent;
import az.ingress.common.saga.events.ReservePaymentEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
@Slf4j
@RequiredArgsConstructor
public class PaymentService {

    private final KafkaTemplate<String, Object> kafkaTemplate;


    @KafkaListener(containerFactory = "reserveInventoryKafkaConsumerFactory", topics = "reserve-payment", groupId = "ingress1")
    public void listenForReserveInventory(ReservePaymentEvent event) throws InterruptedException {
        log.info("Received reserve payment event in payment ms {} ", event);
        log.info("Execute business logic");

        Random rand = new Random();
        if (rand.nextBoolean()) {
            log.info("Enough balance SUCCESS");

        } else {
            log.info("Not enough balance CANCEL");
            CancelShippingEvent cancelShippingEvent = new CancelShippingEvent();
            cancelShippingEvent.setOrderDto(event.getOrderDto());
            kafkaTemplate.send("cancel-shipping", cancelShippingEvent);
        }
    }


}
