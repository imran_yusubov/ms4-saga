package az.ingress.common.saga.events;

import az.ingress.common.saga.dto.OrderDto;
import lombok.Data;

@Data
public class CancelInventoryEvent {


    private OrderDto orderDto;

}
