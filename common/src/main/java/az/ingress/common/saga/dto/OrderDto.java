package az.ingress.common.saga.dto;

import lombok.Data;

import java.util.Map;
import java.util.UUID;

@Data
public class OrderDto {

    private UUID orderId;

    private Map<Long, Integer> products;

    private String address;

}
