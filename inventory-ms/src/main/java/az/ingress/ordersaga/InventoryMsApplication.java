package az.ingress.ordersaga;

import az.ingress.ordersaga.services.InventoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@Slf4j
@SpringBootApplication
@EnableFeignClients
@RequiredArgsConstructor
public class InventoryMsApplication implements CommandLineRunner {

    private final InventoryService inventoryService;

    public static void main(String[] args) {
        SpringApplication.run(InventoryMsApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
      //  inventoryService.reserveInventory();
    }
}
