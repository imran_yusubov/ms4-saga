package az.ingress.ordersaga.controller;

import az.ingress.ordersaga.services.InventoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/orders")
@RequiredArgsConstructor
public class InventroyController {

    private final InventoryService inventoryService;

    @PostMapping
    public void createOrder() {
        inventoryService.reserveInventory();
    }

}
