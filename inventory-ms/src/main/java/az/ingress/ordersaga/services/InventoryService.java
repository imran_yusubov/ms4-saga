package az.ingress.ordersaga.services;

import az.ingress.common.saga.dto.OrderDto;
import az.ingress.common.saga.events.CancelInventoryEvent;
import az.ingress.common.saga.events.ReserveInventoryEvent;
import az.ingress.common.saga.events.ReserveShippingEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Random;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class InventoryService {

    private final KafkaTemplate<String, Object> kafkaTemplate1;

    //Main Operation
    public void reserveInventory() {
        Random rand = new Random();
        if (rand.nextBoolean()) {
            log.info("Enough inventory SUCCESS");
            OrderDto orderDto = new OrderDto();
            log.info("Reserving inventory for order {}", orderDto.getOrderId());
            orderDto.setOrderId(UUID.randomUUID());
            orderDto.setProducts(Map.of(12L, 2, 13L, 1));
            ReserveInventoryEvent reserveInventoryEvent = new ReserveInventoryEvent();
            reserveInventoryEvent.setOrderDto(orderDto);
            kafkaTemplate1.send("reserve-inventory", reserveInventoryEvent);

        } else {
            log.info("Not enough balance CANCEL");
            throw new RuntimeException("Not enough inventory");
        }

    }

    //Compensating Operation
    public void cancelInventory(OrderDto orderDto) {
        log.info("Cancelling inventory for order {}", orderDto.getOrderId());
    }


    @KafkaListener(containerFactory = "reserveInventoryKafkaConsumerFactory", topics = "reserve-inventory", groupId = "ingress1")
    public void listenForReserveInventory(ReserveInventoryEvent event) throws InterruptedException {
        log.info("Received reserve inventory event in inventory ms {} ", event);
        log.info("Execute business logic");

        ReserveShippingEvent reserveShippingEvent = new ReserveShippingEvent();
        reserveShippingEvent.setOrderDto(event.getOrderDto());
        kafkaTemplate1.send("reserve-shipping", reserveShippingEvent);
    }


    @KafkaListener(containerFactory = "cancelInventoryKafkaConsumerFactoryListener", topics = "cancel-inventory", groupId = "ingress1")
    public void cancelInventory(CancelInventoryEvent event) throws InterruptedException {
        log.info("Received cancel inventory event in inventory ms {} ", event);
        log.info("Execute business logic to cancel order {}", event.getOrderDto().getOrderId());

    }

}
