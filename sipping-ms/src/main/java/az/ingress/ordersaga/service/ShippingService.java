package az.ingress.ordersaga.service;

import az.ingress.common.saga.dto.OrderDto;
import az.ingress.common.saga.events.CancelInventoryEvent;
import az.ingress.common.saga.events.CancelShippingEvent;
import az.ingress.common.saga.events.ReservePaymentEvent;
import az.ingress.common.saga.events.ReserveShippingEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Random;

@Slf4j
@Service
@RequiredArgsConstructor
public class ShippingService {

    //Main Operation
    private void makePayment(OrderDto orderDto) {
        log.info("Reserving shipping for order {}", orderDto.getOrderId());
    }

    private final KafkaTemplate<String, Object> kafkaTemplate1;

    @KafkaListener(containerFactory = "reserveInventoryKafkaConsumerFactory", topics = "reserve-shipping", groupId = "ingress1")
    public void reserveShipping(ReserveShippingEvent event) throws InterruptedException {
        log.info("Received reserve shipping event in shipping ms {} ", event);
        log.info("Execute business logic");


        Random rand = new Random();
        if (rand.nextBoolean()) {
            log.info("We ship to this address SUCCESS");
            ReservePaymentEvent reservePaymentEvent = new ReservePaymentEvent();
            reservePaymentEvent.setOrderDto(event.getOrderDto());
            kafkaTemplate1.send("reserve-payment", reservePaymentEvent);
        } else {
            log.info("We don't ship to this address CANCEL");
            CancelInventoryEvent cancelInventoryEvent = new CancelInventoryEvent();
            cancelInventoryEvent.setOrderDto(event.getOrderDto());
            kafkaTemplate1.send("cancel-inventory", cancelInventoryEvent);
        }

    }

    @KafkaListener(containerFactory = "cancelShippingConsumerFactoryListener", topics = "cancel-shipping", groupId = "ingress1")
    public void cancelShipping(CancelShippingEvent event) throws InterruptedException {
        log.info("Received cancel shipping event in shipping ms {} ", event);
        log.info("Execute business logic to cancel the shipping for order {}", event.getOrderDto().getOrderId());

        CancelInventoryEvent cancelInventoryEvent = new CancelInventoryEvent();
        cancelInventoryEvent.setOrderDto(event.getOrderDto());
        kafkaTemplate1.send("cancel-inventory", cancelInventoryEvent);
    }
}
